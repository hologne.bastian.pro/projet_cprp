# Projet cprp

Centralisation via un site web de la gestion du parc de machines-outils des classes CPRP du Lycée Jean-Baptiste de Baudre. Un projet mêlant rétro-ingénierie et génie logiciel.

Projet scolaire, réalisé en 2020 à un niveau BAC +2.